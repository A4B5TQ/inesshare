package checker.controllers;

import checker.constants.AppConstants;
import checker.models.Diff;
import checker.parsers.ExcelReader;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class MainController {

    private final static Map<String, Integer> COLUMNS = new HashMap<String, Integer>() {
        {
            put("A", 0);
            put("B", 1);
            put("C", 2);
            put("D", 3);
            put("E", 4);
            put("F", 5);
            put("G", 6);
            put("H", 7);
            put("I", 8);
            put("J", 9);
            put("K", 10);
            put("L", 11);
            put("M", 12);
            put("N", 13);
            put("O", 14);
            put("P", 15);
            put("Q", 16);
            put("R", 17);
            put("S", 18);
            put("T", 19);
            put("U", 20);
            put("V", 21);
            put("W", 22);
            put("X", 23);
            put("Y", 24);
            put("Z", 25);
        }
    };

    @FXML
    private Label outputText;

    public void fileUpload(ActionEvent actionEvent) {
        Stage currentStage = (Stage) ((Button) actionEvent.getSource()).getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Excel Files", AppConstants.EXCEL_FILE_EXTENSIONS));
        File selectedFile = fileChooser.showOpenDialog(currentStage);

        try {
            if (selectedFile != null) {
                ExcelReader reader = new ExcelReader();
                int cellIndex = this.mapObjectDialogBox();
                Map<String,Diff> res = reader.importExcelFile(selectedFile, cellIndex);
                StringBuilder sb = new StringBuilder();
                res.forEach((key, value) -> {
                    sb.append("Invoices starts with: ").append(key).append(System.lineSeparator());
                    sb.append("Invoice difference: ").append(value.getMissingArguments().size()).append(System.lineSeparator());
                    sb.append("Missing invoice numbers: ").append(value.getMissingArguments().toString().replaceAll("\\[|]", "")).append(System.lineSeparator());
                    sb.append(System.lineSeparator());
                    sb.append(System.lineSeparator());
                });
                this.outputText.setText(sb.toString());
                this.outputText.setMinWidth(Region.USE_PREF_SIZE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int mapObjectDialogBox() {
        Map<Integer, String> objectMap = new HashMap<>();
        ChoiceDialog<String> dialog = new ChoiceDialog<>("A", COLUMNS.keySet());
        dialog.setTitle("Column map dialog");
        dialog.setHeaderText("Choose column that contains current value!");
        dialog.setContentText("Please enter column letter that contains invoice numbers!");
        Optional<String> result = dialog.showAndWait();
        return result.map(COLUMNS::get).orElse(-1);
    }

    private void findDifference() {

    }
}
