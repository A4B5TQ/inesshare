package checker.models;

import java.math.BigInteger;
import java.util.ArrayDeque;
import java.util.Deque;

public class Diff {
    private Deque<BigInteger> invoices;
    private Deque<String> missingArguments;

    public Diff() {
        this.invoices = new ArrayDeque<>();
        this.missingArguments = new ArrayDeque<>();
    }

    public Deque<BigInteger> getInvoices() {
        return this.invoices;
    }

    public Deque<String> getMissingArguments() {
        return this.missingArguments;
    }

    public int getErrorCount() {
        return this.missingArguments.size();
    }
}
