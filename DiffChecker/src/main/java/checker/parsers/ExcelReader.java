package checker.parsers;


import checker.models.Diff;
import checker.parsers.interfaces.Reader;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ExcelReader implements Reader {

    private final static String XLSX = "xlsx";
    private final static String XLS = "xls";

    public Map<String, Diff> importExcelFile(File chooseFile, int cellMap) throws IOException {

        FileInputStream stream = new FileInputStream(chooseFile);
        int dotIndex = chooseFile.getName().lastIndexOf(".");
        String fileType = chooseFile.getName().substring(dotIndex + 1);
        Workbook workbook = null;

        switch (fileType) {
            case XLSX:
                workbook = new XSSFWorkbook(stream);
                break;
            case XLS:
                workbook = new HSSFWorkbook(stream);
                break;
        }

        Sheet sheet = workbook.getSheetAt(0);
        List<String> inputs = new ArrayList<>(sheet.getPhysicalNumberOfRows());
        for (Row row : sheet) {
            if (row.getRowNum() == 0) continue;
            Cell cell = row.getCell(cellMap);
            if (cell == null) break;
            inputs.add(cell.toString().trim());
        }
        inputs.sort((o1, o2) -> {
            int compare = Integer.compare(o2.length(), o1.length());
            if (compare == 0) return o1.compareTo(o2);
            else return compare;
        });
        Map<String, Diff> invoices = new TreeMap<>();
        for (String input : inputs) {
            String key = input.substring(0, 3);
            invoices.putIfAbsent(key, new Diff());
            BigInteger firstElement = invoices.get(key).getInvoices().peekLast();
            if (firstElement != null) {
                BigInteger secondElement = new BigInteger(input);
                int result = Integer.parseInt(secondElement.subtract(firstElement).abs().toString());
                if (result != 1) {
                    String invoiceNum = firstElement.toString();
                    int start = Integer.parseInt(invoiceNum.substring(invoiceNum.length() - String.valueOf(result).length()));
                    int end = Integer.parseInt(input.substring(input.length() - String.valueOf(result).length()));
                    String invoice = invoiceNum.substring(0, invoiceNum.length() - String.valueOf(result).length());
                    int counter = start + 1;
                    for (int i = 0; i < result - 1; i++) {
                        invoices.get(key).getMissingArguments().add(invoice + counter++);
                    }
                }
            }
            invoices.get(key).getInvoices().add(new BigInteger(input));
        }
        return invoices;
    }
}
