package checker.parsers.interfaces;

import checker.models.Diff;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public interface Reader {

    Map<String,Diff> importExcelFile(File chooseFile, int cellMap) throws IOException;
}
