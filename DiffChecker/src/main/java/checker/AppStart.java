package checker;

import checker.constants.AppConstants;
import checker.utils.stageUtils.StageManager;
import checker.utils.stageUtils.StageManagerImpl;
import javafx.application.Application;
import javafx.stage.Stage;

public class AppStart extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        StageManager stageManager = new StageManagerImpl();
        stageManager.loadSceneToStage(primaryStage, AppConstants.MAIN_VIEW_PATH, null);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
