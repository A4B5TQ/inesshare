package checker.constants;

public class AppConstants {

    public static String MAIN_VIEW_PATH = "views/main.fxml";
    public static String APPLICATION_TITLE = "Diff Checker";
    public static final String[] EXCEL_FILE_EXTENSIONS = {"*.xlsx", "*.xls"};

    private AppConstants() {
    }
}
