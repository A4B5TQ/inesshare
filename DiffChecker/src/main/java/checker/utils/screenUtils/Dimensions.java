package checker.utils.screenUtils;

public interface Dimensions {

    double getCurrentDeviceWidth();

    double getCurrentDeviceHeight();
}
